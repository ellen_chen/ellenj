import java.util.Random;
import java.util.Scanner;

public class GuessingGame {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner scan=new Scanner (System.in);
		Random rand = new Random();
		int target=rand.nextInt(100)+1;
		int guess = -1;
		int numOfGuesses = 0;
		while (guess != target) {
			System.out.print("Guess a number from 1-100");
			guess = scan.nextInt();
			numOfGuesses++;
			if (guess>target) {
				System.out.println("Your number is too high try a lower number :P");
			}
			else if (guess<target) {
				System.out.println("Your number is too low try a higher number :P");
			}
			else if (guess==target) {
				System.out.println("You Got The Number!!! You Win! It took you "+numOfGuesses+" Tries.");
			}
			
			
			
		}
		
				
	}

}
